%% normalisation function
function y = normalised_vectors(b,a)
y = b * (max(a)-min(a)) / (max(b)-min(b));
y = y + max(a) - max(y);

%% git
%bitbucket.org (github)
%new one: git init
% git add <filename>
%git commit -a -m "<newversion>"
%on the server: git push

%$ cd /Users/kevinkahn/Documents/MATLAB/
%$ git add *.m 
%$ git commit -a -m "after normalisation"