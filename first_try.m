%% variables
val_psi;
val_delta;
%energy
x;
% parameters: angle of incidence t, refracive incidces n
% o: ambiant (air), 1: thin film, 2 (GaN): substrate (sapphire)
% assuming transparent film, k = 0
t0       = pi/3;
n0      = 1; n1 = 2.399; n2 = 1.77;
d       = 4 * 10^(-6);
lambda   = 632 * 10^(-9);
%data scope
idx_begin       = 100;
idx_end         = 1000;
val_psi_short   = val_psi(idx_begin:idx_end);
val_delta_short = val_delta(idx_begin:idx_end);
x_short         = x(idx_begin:idx_end);

%% equations
% Snell's law
t1 = asin((n0 / n1) * cos(t0));
t2 = asin((n1 / n2) * cos(t1));
% difference in phase
beta = ((2 * pi * d) / lambda) * n1 * cos(t1);
% Fresnel reflection coefficients for ambiant/film/substrate
% p-polarisation component of reflective light waves
rp1  = ((n1 * cos(t0)) - (n0 * cos(t1))) / ((n1 * cos(t0)) + (n0 * cos(t1)));
rs1  = ((n0 * cos(t0)) - (n1 * cos(t1))) / ((n0 * cos(t0)) + (n1 * cos(t1)));
rp2  = ((n2 * cos(t1)) - (n1 * cos(t2))) / ((n2 * cos(t1)) + (n1 * cos(t2)));
rs2  = ((n1 * cos(t1)) - (n2 * cos(t2))) / ((n1 * cos(t1)) + (n2 * cos(t2)));
% three phase model rho
rpmodel = (rp1 + (rp2 * exp(-1i*2*beta))) / (1 + rp1*rp2*exp(-1i*2*beta));
rsmodel = (rs1 + (rs2 * exp(-1i*2*beta))) / (1 + rs1*rs2*exp(-1i*2*beta));
rmodel = rpmodel / rsmodel;
% rho definition
rtheory   = tan(val_psi_short) * exp(1i*val_delta_short);
% complex dielectric function, real and imaginary parts
%e   = ((sin(t0))^2) + ((sin(t0))^2) * ((tan(t0))^2) * (((1-rmodel)/(1+rmodel))^2);
%e1  = real(e);
%e2  = imag(e);
% refractive index and absorption coefficient
%n   = sqrt((e1+sqrt((e1.^2)+(e2.^2)))/2);
%k   = sqrt((-e1+sqrt((e1.^2)+(e2.^2)))/2);

%% figures

%axes label
a='energy [eV]';
b='psi [deg]';
c='delta [deg]';
rho= 'rho';

% normalisaton psi and delta
val_delta_norm  = normalised_vectors(val_delta_short, val_psi_short);

% psi and delta
figure;
hold on
plotyy(x_short, val_psi_short, x_short,val_delta_short);
xlabel(a);
ylabel(b);
ylabel(c);
hold off;

% rho
figure;
hold on;
plot(x_short,rmodel);
%plot(x_short,rtheory);
xlabel(a);
ylabel(rho);
hold off;
% dielectric function
%figure;
%h2 = plot(x_short,e);
% refractive index
%figure;
%h3 = plot(x,n);

%% just for fun
%[x1,y1] = meshgrid(-5:0.1:5,-5:0.1:5);
%rr = 10; ss = 2;
%z1 = (x1.^2)/rr^2 - (y1.^2)/ss^2;
%surf(x1,y1,z1)

%h   = 4.1343359*10^(-15); %eV?s
%c   = 3 *10^8;
%l   = hc/x;